using System.Collections;
using System.Collections.Generic;
using BooksApi.Models;

namespace BooksApi.Repositories
{
    public interface IGenericRepository<T> where T : Entity
    {
        List<T> GetAll();
        T GetById(string Id);
        void Create(T entity);
        void Update(string Id, T entity);
        void Delete(string Id);

    }
}