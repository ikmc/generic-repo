using System;
using System.Collections;
using System.Collections.Generic;
using BooksApi.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BooksApi.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : Entity
    {
        private readonly IMongoCollection<T> _entities;

        public GenericRepository(IBookstoreDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _entities = database.GetCollection<T>(settings.BooksCollectionName);
        }
        
        public List<T> GetAll()
        {
            List<T> allEntities = _entities.Find(entity => true).ToList();
            //throw new System.NotImplementedException();
            return allEntities;
        }

        public T GetById(string Id)
        {
            var objectId = new ObjectId(Id);
            var filter = Builders<T>.Filter.Eq(doc => doc.Id, objectId);
            return _entities.Find(filter).SingleOrDefault();
            //T theEntity = _entities.Find<T>(entity => entity.Id == Id).FirstOrDefault();
            //return theEntity;
            //throw new System.NotImplementedException();
        }

        public void Create(T entity)
        {
            _entities.InsertOne(entity);
            //throw new System.NotImplementedException();
        }

        public void Update(string Id, T entityUp)
        {
            var objectId = new ObjectId(Id);
            var filter = Builders<T>.Filter.Eq(doc => doc.Id, objectId);
            entityUp.UpdatedAt = DateTime.Now;
            _entities.FindOneAndReplace(filter, entityUp);
            //throw new System.NotImplementedException();
        }

        public void Delete(string Id)
        {
            var objectId = new ObjectId(Id);
            var filter = Builders<T>.Filter.Eq(doc => doc.Id, objectId);
            _entities.FindOneAndDelete(filter);
            //throw new System.NotImplementedException();
        }
    }
}