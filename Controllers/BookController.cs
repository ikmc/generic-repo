using System.Collections.Generic;
using BooksApi.Models;
using BooksApi.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace BooksApi.Controllers
{
    [Route("api/books")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private IGenericRepository<Book> bookRepository;

        public BookController(IGenericRepository<Book> bookRepository)
        {
            this.bookRepository = bookRepository;
        }
        
        [HttpGet]
        public ActionResult<List<Book>> Get()
        {
            return bookRepository.GetAll();
        }

        [HttpGet("{id:length(24)}", Name = "GetBook")]
        public ActionResult<Book> Get(string Id)
        {
            var book = bookRepository.GetById(Id);
            if (book == null)
            {
                return NotFound();
            }

            return book;
        }

        [HttpPost]
        public ActionResult<Book> Create(Book book)
        {
            bookRepository.Create(book);
            return CreatedAtRoute("GetBook", new { id = book.Id.ToString() }, book);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, Book bookUp)
        {
            var book = bookRepository.GetById(id);
            if (book == null)
            {
                return NotFound();
            }

            bookRepository.Update(id, bookUp);
            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var book = bookRepository.GetById(id);
            if (book == null)
            {
                return NotFound();
            }

            bookRepository.Delete(id);
            return NoContent();
        }
        
    }
}